job "redis" {
    datacenters = ["dc1"]
    type = "service"
    group "redis" {
        network {
            port "redis" {
                static = 6379
                to = 6379
            }
        }
        service {
            name = "redis"
            port = "redis"
        }
        task "redis" {
            vault {
                policies = ["redis"]
            }
			template {
				data = file("cert.pem.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("key.pem.tpl")
				destination = "secrets/key.pem"
			}
			template {
				data = file("ca_chain.pem.tpl")
				destination = "local/ca_chain.pem"
			}
            template {
                data = file("config.conf.tpl")
                destination = "local/config.conf"
            }
            driver = "docker"
            config {
                image = "redis:6-alpine"
                command = "redis-server"
                args = ["/local/config.conf"]
                ports = ["redis"]
                sysctl = {
                    "net.core.somaxconn" = "16384"
                }
            }
            volume_mount {
                volume = "data"
                destination = "/data"
            }
        }
        volume "data" {
            type = "host"
            source = "redis"
            read_only = false
        }
    }
}
