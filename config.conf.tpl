bind 0.0.0.0
protected-mode no
port 0
tls-port 6379
tls-cert-file /local/cert.pem
tls-key-file /secrets/key.pem
tls-ca-cert-file /local/ca_chain.pem
tls-auth-clients optional
tls-cluster yes
tls-protocols "TLSv1.2 TLSv1.3"
always-show-logo no
appendonly yes
save 3600 1
save 300 100
save 60 10000
user default off nopass ~* &* +@all
user admin on #3a360e1f70e70c71047f7af24f3e429b07ff86c1c8a0edb313a6e00bcd555a49 ~* &* +@all
user weblate on #bfeb38ee373c313dc68ab9acfce8f85847b34812678a7e3be5d5218d2d909574 ~* &* +@all -config
dir "/data"
