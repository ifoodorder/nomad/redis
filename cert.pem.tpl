{{ with $ip_address := (env "NOMAD_HOST_IP_redis") }}
{{ with secret "pki_int/issue/cert" "role_name=redis" "common_name=redis.service.consul" "ttl=24h" "alt_names=_redis._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ end }}{{ end }}
